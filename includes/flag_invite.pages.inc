<?php

/**
 * Menu callback; Display the settings form for Flag Invite.
 */
function flag_invite_settings_form($form, &$form_state) {
  global $user; 
  
  // Check to see if there are any user flag types available.
  if ($user_flags = flag_get_flags('user', NULL, $user)) {
    $options = array();
    foreach ($user_flags as $fid => $flag) {
      $options[$flag->fid] = $flag->title;
    }

    $form['flag_invite_flag_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t("Flag types available on the invite form."),
      '#description' => t("Please select the flag types that you would like to make available on the !link. Please not that only flags that target users will be available. If none are selected, all flags eligible flags will be enabled.", array('!link' => l('invite form', 'invite'))),
      '#options' => $options,
      '#default_value' => variable_get('flag_invite_flag_types', array()),
    );
  }
  else {
    drupal_set_message(t('You do not currently have any flag types that allow the flagging of users.'));
  }
  
  $form['custom_text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom labels and help text.'),
    '#description' => t("Customize the labels and help text for Flag Invite fields on the invite form."),
  );
  $form['custom_text']['flag_invite_checkbox_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkbox label'),
    '#description' => t("Customize the text that appears next to the <em>Flag invitee</em> checkbox on the !link.", array('!link' => l('invite form', 'invite'))),
    '#default_value' => variable_get('flag_invite_checkbox_label', t('Flag users that accept your invitation')),
  );
  $form['custom_text']['flag_invite_checkbox_desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkbox help text'),
    '#description' => t("Customize the help text (description) that appears under the <em>Flag invitee</em> checkbox on the !link.", array('!link' => l('invite form', 'invite'))),
    '#default_value' => variable_get('flag_invite_checkbox_desc', t('When the invitee responds to your invitation and registers, they will automatically be flagged for you.')),
  );
  $form['custom_text']['flag_invite_type_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag type label'),
    '#description' => t("Customize the text that appears next to the <em>Flag type</em> radios on the !link.", array('!link' => l('invite form', 'invite'))),
    '#default_value' => variable_get('flag_invite_type_label', t("Flag type")),
  );
  $form['custom_text']['flag_invite_type_desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag type help text'),
    '#description' => t("Customize the help text (description) that appears under the <em>Flag type</em> radios on the !link.", array('!link' => l('invite form', 'invite'))),
    '#default_value' => variable_get('flag_invite_type_label', t("Please select the type of flag that you'd like to use")),
  );

  return system_settings_form($form);
}